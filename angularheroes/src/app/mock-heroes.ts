import {Hero} from './Hero'

export  const HEROES: Hero[]= [
  {
    id: 1,
    firstName: 'Clark',
    lastName: 'Kent',
    heroName: 'Super Man' ,
    nameSuperPower: 'super speed'
  },
  {
    id: 2,
    firstName: 'Paul',
    lastName: 'Shan',
    heroName: 'Green Lantern',
    nameSuperPower: 'super speed'
  },
  {
    id: 3,
    firstName: 'Marvel',
    lastName: 'Comis',
    heroName: 'Zoro',
    nameSuperPower: 'mind control'
  }

]
