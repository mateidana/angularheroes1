export interface Hero {
  id?: number;
  firstName: string;
  lastName: string;
  heroName: string;
  nameSuperPower: string;
}
