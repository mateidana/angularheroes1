import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Hero} from '../../Hero';
import { HeroModalComponent } from '../hero-modal/hero-modal.component';
import { UiService } from 'src/app/services/ui.service';



@Component({
  selector: 'app-hero-item',
  templateUrl: './hero-item.component.html',
  styleUrls: ['./hero-item.component.css']
})
export class HeroItemComponent implements OnInit {
@Input() hero!: Hero;
@Output() delete: EventEmitter<Hero> = new EventEmitter();
@Output() edit: EventEmitter<Hero> = new EventEmitter();
showHeroList: boolean =false;
  modalService: any;
  subscription: any;


  constructor(
    private uiService: UiService
  ){}

  ngOnInit(): void {
  //   this.subscription =this.uiService.onToggle().subscribe((value: any)=> (this.showHeroList = value));
  }

  onDelete(){
    this.delete.emit(this.hero);
  }

  onEdit(){
  this.edit.emit(this.hero);
}
 }



