import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Hero} from '../../Hero';
import { UiService } from '../../services/ui.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-hero',
  templateUrl: './add-hero.component.html',
  styleUrls: ['./add-hero.component.css']
})
export class AddHeroComponent implements OnInit {
  @Output() onAddHero: EventEmitter<Hero>= new EventEmitter();
  heroName!: string;
  firstName!: string;
  lastName!: string;
  nameSuperPower!: string;
  showAddHero!: boolean;
  subscription: Subscription;


  constructor(private uiService: UiService) {
    this.subscription = this.uiService
    .onToggle().
    subscribe((value)=> (this.showAddHero= value));

  }

  ngOnInit(): void {
  }

onSubmit(){
  if(!this.heroName) {
    alert('Completeza numele supereroului');
    return;
}
const newHero = {
  heroName: this.heroName,
  firstName: this.firstName,
  lastName: this.lastName,
  nameSuperPower: this.nameSuperPower
};

this.onAddHero.emit(newHero);
this.heroName='';
this.firstName='';
this.lastName='';
this.nameSuperPower='';
}
}
