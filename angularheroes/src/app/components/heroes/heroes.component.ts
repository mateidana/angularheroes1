import { Component, OnInit } from '@angular/core';
import {HeroService} from '../../services/hero.service';
import {Hero} from '../../Hero';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { HeroModalComponent } from '../hero-modal/hero-modal.component';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[] = [];

  constructor(private heroService: HeroService,
    private modalService: NgbModal ) { }

  ngOnInit(): void {
    this.heroService.getHero().subscribe((heroes) =>this.heroes= heroes );
  }
deleteHero(hero: Hero) {
  this.heroService.deleteHero(hero).subscribe(() =>this.heroes= this.heroes.filter(h => h.id !== hero.id) );
}

addHero(hero:Hero) {
  this.heroService.addHero(hero).subscribe((hero) =>this.heroes.push(hero) );
}

showEditHero(hero: Hero) {
  const modalRef = this.modalService.open(HeroModalComponent);
  modalRef.componentInstance.clonedHero = {...hero};
  modalRef.closed.subscribe(updatedHero => {
    this.heroService.updateHero(updatedHero).subscribe(() => {
      this.heroes = this.heroes.map((h) =>{
        if (h.id ===updatedHero.id) {
          return updatedHero;
        }
        return h;
      })
    });
  });
}

}

