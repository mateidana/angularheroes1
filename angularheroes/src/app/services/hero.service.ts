import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Hero} from '../Hero';
import { HeroItemComponent } from '../components/hero-item/hero-item.component';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class HeroService {



 private apiURL ='http://localhost:5000/heroes'

  constructor(private http:HttpClient) { }

getHero(): Observable<Hero[]> {
return this.http.get<Hero[]>(this.apiURL);
}

deleteHero(hero: Hero): Observable<Hero> {
const url= `${this.apiURL}/${hero.id}`;
return this.http.delete<Hero>(url);
}

addHero(hero: Hero): Observable<Hero> {
  return this.http.post<Hero>(this.apiURL, hero);
}

updateHero(hero: Hero): Observable<Hero> {
  const url= `${this.apiURL}/${hero.id}`;
  return this.http.patch<Hero>(url, hero, httpOptions);
  }
}

